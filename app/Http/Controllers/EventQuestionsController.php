<?php

namespace App\Http\Controllers;

use App\Attendize\Utils;
use App\Models\Attendee;
use App\Models\Event;
use App\Models\Order;
use App\Models\QuestionAnswer;
use App\Models\Question;
use Illuminate\Http\Request;

class EventQuestionsController extends Controller
{
	public function showQuestionsLogin(Request $request, $event_id, $slug = '') {
		$event = Event::findOrFail($event_id);

		if (!Utils::userOwns($event) && !$event->is_live) {
			return view('Public.ViewEvent.EventNotLivePage');
		}

		$error = session('EventQuestionsLoginProblem');

		$data = [
			'event'       => $event,
			'is_embedded' => 0,
			'error' => $error
		];

		return view('Public.ViewEvent.EventQuestionsLogin', $data);
	}

	public function postQuestionsLogin(Request $request, $event_id, $slug='') {
		$rules = [
			'attendeeref'    => 'required',
			'last_name'   => ['required']
		];

		$this->validate($request, $rules);

		$refArray = explode('-', $request->input('attendeeref'));
		$lastName = $request->input('last_name');

		$refValid = true;
		if (count($refArray) != 2) {
			$refValid = false;
		} elseif (!intval($refArray[1])) {
			$refValid = false;
		}

		if (!$refValid) {
			session()->flash('EventQuestionsLoginProblem', 'There was a problem with your login data.');
			return redirect()->to(route('showEventQuestionsPage', [$event_id]));

		}

		$order = Order::where('order_reference', $refArray[0])->first();

		if (!$order) {
			session()->flash('EventQuestionsLoginProblem', 'There was a problem with your login data.');
			return redirect()->to(route('showEventQuestionsPage', [$event_id]));
		}

		$attendee = Attendee::where([
			'order_id' => $order->id,
			'reference_index' => intval($refArray[1]),
			'last_name' => $lastName
		])->get();

		if (!$attendee->first()) {
			session()->flash('EventQuestionsLoginProblem', 'There was a problem with your login data.');
			return redirect()->to(route('showEventQuestionsPage', [$event_id]));
		}

		$ids = $attendee->map(function ($item, $key) {
			return $item->id;
		});

		session()->set('EventQuestionsAttendee', $ids);

		return redirect()->to(route('showAttendeeQuestionsPage', [$event_id, 'questions']));
	}

	public function showAttendeeQuestions(Request $request, $event_id) {
		$attendeeids = session('EventQuestionsAttendee');
		if ($attendeeids) $attendeeids = $attendeeids->toArray();
		$attendeeData = Attendee::find($attendeeids);
		if (!$attendeeData) {
			return redirect()->to(route('showEventQuestionsPage', [$event_id]));
		}

		$data = $this->getQuestionViewData($event_id, $attendeeData);

		return view('Public.ViewEvent.EventQuestionsDisplay', $data);
	}

	public function postAnswers(Request $request, $event_id) {
		$answers = $request->input('ticket_holder_questions');
		$attendeeIDs = $request->input('attendee_id');

		foreach ($answers as $ticketID => $answerArray) {
			$attendeeID = $attendeeIDs[$ticketID];
			foreach ($answerArray as $questionID => $answer) {
				$q = Question::findOrFail($questionID);

				$qa = QuestionAnswer::where([
					'attendee_id' => intval($attendeeID),
					'event_id'    => intval($event_id),
					'question_id' => intval($questionID),
					'account_id'  => $q->account_id
				])->first();

				if ( ! $qa) {
					$qa              = new QuestionAnswer();
					$qa->attendee_id = intval($attendeeID);
					$qa->event_id    = intval($event_id);
					$qa->question_id = intval($questionID);
					$qa->account_id  = $q->account_id;
				}

				$qa->answer_text = $answer;
				$qa->save();
			}
		}

		$message = 'Thank you, your data has been saved. Please log out below.';

		$data = $this->getQuestionViewData($event_id, Attendee::find($attendeeIDs), $message);

		return view('Public.ViewEvent.EventQuestionsDisplay', $data);
	}

	public function logoutQuestions($event_id) {
		session()->clear();
		return redirect()->to(route('showEventQuestionsPage', [$event_id]));
	}

	protected function getQuestionViewData($event_id, $attendeeData, $message = null) {
		$event = Event::findOrFail($event_id);
		$tickets = array();
		$questions = array();
		$attendees = array();
		//todo: get all assigned questions for all attendee tickets and display them in view
		foreach ($attendeeData as $attendee) {
			$attendees[] = $attendee;
			foreach ($attendee->ticket()->getResults()->questions()->where('is_enabled', 1)->orderBy('sort_order')->get() as $question) {
				$questions[$question->id] = $question;
			}
		}
		if (!isset($attendee)) return [];

		$data = [
			'event'        => $event,
			'is_embedded'  => 0,
			'attendees'    => $attendees,
			'tickets'      => $tickets,
			'questions'    => $questions,
			'brewManiaLink' => self::getBrewmaniaRegisterLink($attendee),
			'message' => $message
		];

		return $data;
	}

	public static function getBrewmaniaRegisterLink($attendee) {
		$attendeeRef = $attendee->order()->getResults()->order_reference . '-' . $attendee->reference_index;
		$attendeeFirstName = $attendee->first_name;
		$attendeeSurname = $attendee->last_name;
		$attendeeEmail = $attendee->email;

		$link = sprintf(
			'brewmania.nzhc.nz/register/?TicketReference=%s&FirstName=%s&Surname=%s&Email=%s',
			$attendeeRef,
			$attendeeFirstName,
			$attendeeSurname,
			$attendeeEmail
		);
		return $link;
		//
	}
}