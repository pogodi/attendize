<?php

namespace App\Http\Controllers;

use App\Attendize\Utils;
use App\Models\Affiliate;
use App\Models\Event;
use App\Models\EventStats;
use App\Models\Attendee;
use App\Models\Order;
use Auth;
use Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mail;
use Validator;
use Carbon\Carbon;

class EventViewController extends Controller
{
    /**
     * Show the homepage for an event
     *
     * @param Request $request
     * @param $event_id
     * @param string $slug
     * @param bool $preview
     * @return mixed
     */
    public function showEventHome(Request $request, $event_id, $slug = '', $preview = false)
    {
        $event = Event::findOrFail($event_id);

        if (!Utils::userOwns($event) && !$event->is_live) {
            return view('Public.ViewEvent.EventNotLivePage');
        }

        $tickets = $event->tickets()->where(function ($query) {
	        $query->whereNull('start_sale_date')->orWhere('start_sale_date', '<=', Carbon::create());
        })->orderBy('use_ticket_code', 'created_at', 'desc')->get();

        $ticket_code_tickets = array_filter($tickets->toArray(), function($ticketItem) {
        	return $ticketItem['use_ticket_code'] ? true: false;
        });

	    $ticketCode = null;
	    $attendee = null;
        if (count ($ticket_code_tickets)) {
	        $ticketCode = session()->get('TicketCode');
	        if (!$ticketCode) {
		        return redirect()->to(route('registerTicketCode', [$event_id, 'nzhc']));
	        }
	        $attendee = Attendee::where('ticket_sales_code', $ticketCode)->first();
        }

	    $attendee_ticket_ids = array();
	    if ($attendee) {
        	// get order by attendee
		    // get attendees by order -> they link to tickets
		    $order = $attendee->order()->getResults();
		    $attendees = $order->attendees()->getResults()->toArray();
		    $attendee_ticket_ids = array_map(function($el) {
		    	return $el['ticket_id'];
		    }, $attendees);
	    }

	    $data = [
            'event'       => $event,
            'tickets'     => $tickets,
		    'bought'   => $attendee_ticket_ids,
            'is_embedded' => 0,
		    'registered'  => ($attendee ? true: false)
        ];
        /*
         * Don't record stats if we're previewing the event page from the backend or if we own the event.
         */
        if (!$preview && !Auth::check()) {
            $event_stats = new EventStats();
            $event_stats->updateViewCount($event_id);
        }

        /*
         * See if there is an affiliate referral in the URL
         */
        if ($affiliate_ref = $request->get('ref')) {
            $affiliate_ref = preg_replace("/\W|_/", '', $affiliate_ref);

            if ($affiliate_ref) {
                $affiliate = Affiliate::firstOrNew([
                    'name'       => $request->get('ref'),
                    'event_id'   => $event_id,
                    'account_id' => $event->account_id,
                ]);

                ++$affiliate->visits;

                $affiliate->save();

                Cookie::queue('affiliate_'.$event_id, $affiliate_ref, 60 * 24 * 60);
            }
        }

        return view('Public.ViewEvent.EventPage', $data);
    }

    /**
     * Show preview of event homepage / used for backend previewing
     *
     * @param $event_id
     * @return mixed
     */
    public function showEventHomePreview($event_id)
    {
        return showEventHome($event_id, true);
    }

    /**
     * Sends a message to the organiser
     *
     * @param Request $request
     * @param $event_id
     * @return mixed
     */
    public function postContactOrganiser(Request $request, $event_id)
    {
        $rules = [
            'name'    => 'required',
            'email'   => ['required', 'email'],
            'message' => ['required'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validator->messages()->toArray(),
            ]);
        }

        $event = Event::findOrFail($event_id);

        $data = [
            'sender_name'     => $request->get('name'),
            'sender_email'    => $request->get('email'),
            'message_content' => strip_tags($request->get('message')),
            'event'           => $event,
        ];

        Mail::send('Emails.messageOrganiser', $data, function ($message) use ($event, $data) {
            $message->to($event->organiser->email, $event->organiser->name)
                ->from(config('attendize.outgoing_email_noreply'), $data['sender_name'])
                ->replyTo($data['sender_email'], $data['sender_name'])
                ->subject('Message Regarding: '.$event->title);
        });

        return response()->json([
            'status'  => 'success',
            'message' => 'Message Successfully Sent',
        ]);
    }
}
