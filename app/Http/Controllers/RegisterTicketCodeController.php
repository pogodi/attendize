<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Attendize\Utils;
use App\Models\Event;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class RegisterTicketCodeController extends Controller
{
	public function registerTicketCode(Request $request, $event_id, $slug = '') {
		$event = Event::findOrFail($event_id);

		if (!Utils::userOwns($event) && !$event->is_live) {
			return view('Public.ViewEvent.EventNotLivePage');
		}

		$error = session('RegisterTicketCodeProblem');

		$data = [
			'event'       => $event,
			'is_embedded' => 0,
			'error' => $error
		];

		return view('Public.ViewEvent.RegisterTicketCode', $data);
	}

	public function postTicketCode(Request $request, $event_id, $slug='') {
		$rules = [
			'ticket_code'    => 'required'
		];

		$this->validate($request, $rules);

		$ticket_code = $request->input('ticket_code');

		$exists = DB::connection('extmysql')->select('SELECT * FROM TicketCode WHERE Code = :code', ['code' => $ticket_code]);

		if (!$ticket_code) {
			session()->flash('RegisterTicketCodeProblem', 'Please enter your ticket code (on the back of your ticket).');
			return redirect()->to(route('registerTicketCode', [$event_id]));
		}
		if (!count($exists)) {
			session()->flash('RegisterTicketCodeProblem', 'We could not find your ticket code in our system.');
			return redirect()->to(route('registerTicketCode', [$event_id]));
		}

		session()->set('TicketCode', $ticket_code);

		return redirect()->to(route('showEventPage', [$event_id, 'nzhc']));
	}
}
