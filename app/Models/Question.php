<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Questions.
 *
 * @author Dave
 */
class Question extends MyBaseModel
{
    use SoftDeletes;

    /**
     * The events associated with the question.
     *
     * @access public
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany('\App\Models\Event');
    }

    /**
     * The type associated with the question.
     *
     * @access public
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function question_type()
    {
        return $this->belongsTo('\App\Models\QuestionType');
    }

    public function answers()
    {
        return $this->hasMany('\App\Models\QuestionAnswer');
    }

    /**
     * The options associated with the question.
     *
     * @access public
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function options()
    {
        return $this->hasMany('\App\Models\QuestionOption');
    }

    public function tickets()
    {
        return $this->belongsToMany('\App\Models\Ticket');
    }

    /**
     * Scope a query to only include active questions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }

	public function getSurveyAnswers($event_id) {
		$rows[] = [
			'Order Ref',
			'Attendee Name',
			'Attendee Email',
			'Attendee Ticket',
			$this->title
		];

		$event = Event::scope()->findOrFail($event_id);

		$attendees = $event->attendees()->has('answers')->get();

		foreach ($attendees as $attendee) {

			if (in_array($this->id, $attendee->answers->lists('question_id')->toArray())) {
				$answer = $attendee->answers->where('question_id', $this->id)->first()->answer_text;
			} else {
				$answer = null;
			}

			$rows[] = [
				$attendee->order->order_reference,
				$attendee->full_name,
				$attendee->email,
				$attendee->ticket->title,
				$answer
			];

		}

		return $rows;
	}

	public function getAttendeesUnanswered($event_id) {
		// get all tickets with this question attached
		// get all attendees with those tickets
		// check if they have answered this question, at least once
		$tickets = $this->tickets();
		$return = [];
		$test = $this->title;
		$answered = [];

		$answers = $this->answers;
		foreach ($answers as $answer) {
			$attendee = $answer->attendee;
			if (!empty(trim($answer->answer_text))) {
				$answered[$attendee->order_id.'-'.$attendee->reference_index] = $answer;
			}
		}

		foreach ($tickets->get() as $ticket) {
			$attendees = Attendee::where('ticket_id', $ticket->id)->where('is_cancelled', 0)->get();
			if (!$attendees) continue;
			foreach ($attendees as $attendee) {
				$answer = QuestionAnswer::where('attendee_id', '=', $attendee->id)
				                        ->where('question_id', '=', $this->id)
				                        ->where('event_id', '=', $event_id)
				                        ->first();
				if (!$answer || empty(trim($answer->answer_text))) {
					$return[$attendee->order_id.'-'.$attendee->reference_index] = $attendee;
				}
			}
		}
		$return = array_diff_key($return, $answered);

		return new Collection($return);
	}

	public function getAnswersExpected() {
		// get all tickets with this question attached
		// get all attendees with those tickets
		$tickets = $this->tickets();
		$return = [];

		foreach ($tickets->get() as $ticket) {
			$attendees = Attendee::where('ticket_id', $ticket->id)->where('is_cancelled', 0)->get();
			if (!$attendees) continue;
			foreach ($attendees as $attendee) {
				$return[$attendee->order_id.'-'.$attendee->reference_index] = $attendee;
			}
		}

		return new Collection($return);
	}

}
