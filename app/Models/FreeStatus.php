<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreeStatus extends Model
{

	/**
	 * The validation rules
	 *
	 * @var array $rules
	 */
	protected $rules = [
		'name' => ['required']
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array $fillable
	 */
	protected $fillable = [
		'name',
	];

	public $timestamps = false;
}
