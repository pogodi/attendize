<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreeTicketStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('free_statuses', function($table) {
		    $table->increments('id');
		    $table->string('name');
	    });

        //add status field
	    Schema::table('attendees', function (Blueprint $table) {
		    $table->unsignedInteger('free_status_id')->nullable()->index();
		    $table->foreign('free_status_id')->references('id')->on('free_statuses')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

	    Schema::table('attendees', function (Blueprint $table) {
		    $table->dropForeign('attendees_free_status_id_foreign');
	    	$table->dropIndex('attendees_free_status_id_index');
		    $table->dropColumn('free_status_id');
	    });

	    Schema::drop('free_statuses');
    }
}
