<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketSalesCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('attendees', function (Blueprint $table) {
		    $table->char('ticket_sales_code', 20)->nullable();
	    });

	    Schema::table('tickets', function (Blueprint $table) {
		    $table->boolean('use_ticket_code');
	    });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('attendees', function (Blueprint $table) {
		    $table->dropColumn('ticket_sales_code');
	    });

	    Schema::table('tickets', function (Blueprint $table) {
		    $table->dropColumn('use_ticket_code');
	    });
    }
}
