<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketBundles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('ticket_bundles', function($table) {
		    $table->increments('id');
		    $table->integer('ticket_id')->unsigned()->index();
		    $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('cascade');
		    $table->integer('bundle_id')->unsigned()->index();
		    $table->foreign('bundle_id')->references('id')->on('tickets')->onDelete('cascade');
	    });

	    Schema::table('tickets', function (Blueprint $table) {
		    $table->boolean('is_bundle');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('ticket_bundles');

	    Schema::table('tickets', function (Blueprint $table) {
		    $table->dropColumn('is_bundle');
	    });
    }
}
