<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubAttendees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // parent-child relation for attendees (for sub tickets)
	    Schema::table('attendees', function (Blueprint $table) {
		    $table->unsignedInteger('parent_id')->nullable()->index();
		    $table->foreign('parent_id')->references('id')->on('attendees')->onDelete('cascade');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	    Schema::table('attendees', function (Blueprint $table) {
		    $table->dropForeign('attendees_parent_id_foreign');
		    $table->dropIndex('attendees_parent_id_index');
		    $table->dropColumn('parent_id');
	    });
    }
}
