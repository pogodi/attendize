@extends('Emails.Layouts.Master')

@section('message_content')
Hello {{$attendee->first_name}},<br><br>

Thank you for taking part in the <b>{{$attendee->order->event->title}}</b>!<br/><br/>

We have some questions for you we need answered to make sure the event can go ahead smoothly. <br/>
You might get this email every few days until all questions are answered. <br/>
Sorry about that, but all the answers are needed for either legal reasons or to deliver the product you paid for.<br/>
<br/>
Please click on the following link to proceed (we will log you in automatically to make it as easy as possible).<br/>
<br/>
<a href="{{$link}}">{{$link}}</a>

<br><br>
Regards
@stop
