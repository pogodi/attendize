@extends('Emails.Layouts.Master')

@section('message_content')
    Hello {{$attendee->first_name}},<br><br>

    Thank you for taking part in BrewMania at <b>{{$attendee->order->event->title}}</b>!<br/><br/>

    Please register your beer entries by clicking on the link below.<br/>
    <strong>Don't for get to label your bottles with the Entry ID's you receive after registration!</strong><br/>

    <br/><br/>
    <a href="http://{{$link}}">{{$link}}</a>

    <br><br>
    Regards
@stop
