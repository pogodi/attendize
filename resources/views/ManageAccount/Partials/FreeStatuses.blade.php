{!! Form::model($freestatus, array('url' => route('postCreateFreeStatus'), 'class' => 'ajax ')) !!}
<div class="table-responsive">
    <table class="table table-bordered">
        <tbody>
        @foreach($freestatuses as $status)
            <tr>
                <td>
                    {{$status->name}}
                </td>
                <td>
                    <a href="javascript:;" class="deleteThis" data-route='{{$freestatusdeletelink}}' data-id='{{$status->id}}' data-type='FreeStatus'>delete</a>
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="3">
                <div class="input-group">
                    {!! Form::text('name', '',  ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                    <span class="input-group-btn">
                        {!!Form::submit('Add Status', ['class' => 'btn btn-primary'])!!}
                    </span>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>


{!! Form::close() !!}