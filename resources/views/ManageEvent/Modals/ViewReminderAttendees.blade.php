<div role="dialog" class="modal fade" style="display: ;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    Attendees with open questions
                </h3>
            </div>

            @if(count($attendees))
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                Reference
                            </th>
                            <th>
                                Attendee
                            </th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($attendees as $attendee)
                            <tr>
                                <td>
                                    {{ $attendee->order->order_reference }}
                                </td>

                                <td>
                                    {{ $attendee->full_name }}
                                    @if($attendee->is_cancelled)
                                        (<span title="This attendee has been cancelled" class="text-danger">Cancelled</span>)
                                    @endif<br>
                                    <a title="Go to attendee: {{ $attendee->full_name }}" href="{{route('showEventAttendees', ['event_id' => $attendee->event_id, 'q' => $attendee->reference])}}">{{ $attendee->email }}</a><br>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            @else
                <div class="modal-body">
                    <div class="alert alert-info">
                        All Attendees have answered all questions! Whoohoo!
                    </div>
                </div>

            @endif

            <div class="modal-footer">
                {!! Form::button('Close', ['class'=>"btn modal-close btn-danger",'data-dismiss'=>'modal']) !!}
            </div>
        </div><!-- /end modal content-->
    </div>
</div>