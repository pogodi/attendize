@extends('Public.ViewEvent.Layouts.EventPage')

@section('content')
    @include('Public.ViewEvent.Partials.EventHeaderSection')
    @include('Public.ViewEvent.Partials.RegisterTicketCodeSection')
    @include('Public.ViewEvent.Partials.EventFooterSection')
@stop