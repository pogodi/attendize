<section id="questions_login" class="container">
    <div class="row">
        <h1 class='section_head'>
            Please fill out all questions below:
        </h1>
    </div>
    @if($message)
    <div class="row">
        <div class="col-md-12">
            <div class="content">
                <h3 class="notice">{{$message}}</h3>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="content">
                @include('Public.ViewEvent.Partials.AttendeeQuestion', ['attendees' => $attendees, 'questions' => $questions])
            </div>
        </div>
    </div>
</section>