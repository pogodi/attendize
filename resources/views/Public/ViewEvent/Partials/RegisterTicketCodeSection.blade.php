<section id="questions_login" class="container">
    <div class="row">
        <h1 class='section_head'>
            Register your ticket
        </h1>
        <div class="col-sm-12">{!! Markdown::parse($event->description) !!}</div>
        <span class="error">{{$error}}</span>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="content">
                {!! Form::open(['url' => route('postTicketCode', ['event_id' => $event->id])]) !!}

                <div class="form-group">
                    {!! Form::label('Ticket Code') !!}
                    {!! Form::text('ticket_code', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Ticket Code')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Continue',
                      array('class'=>'btn btn-primary')) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>