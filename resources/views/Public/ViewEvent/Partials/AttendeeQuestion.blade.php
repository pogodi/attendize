{!! Form::open(['url' => route('postAnswers', ['event_id' => $event->id])]) !!}

@foreach($attendees as $attendee)
    <?php
        $ticket = $attendee->ticket()->first();
        if ($ticket->is_bundle) continue;
    ?>
    @if(count($attendee->ticket()->getResults()->questions()->where('is_enabled', 1)->orderBy('sort_order')->get()))
        <h3>{{$ticket->title}}</h3>
        {!! Form::hidden("attendee_id[{$ticket->id}]", $attendee->id) !!}

    @endif

    @foreach($attendee->ticket()->getResults()->questions()->where('is_enabled', 1)->orderBy('sort_order')->get() as $question)
    <?php
    $answer = $question->answers->where('attendee_id', $attendee->id)->first();
    $answer = isset($answer->answer_text) ? $answer->answer_text : '';
    ?>
    <div class="col-xs-12">
        <div class="form-group">
            {!! Form::label("ticket_holder_questions[{$ticket->id}][$question->id]", $question->title, ['class' => $question->is_required ? 'required' : '']) !!}

            @if($question->question_type_id == config('attendize.question_textbox_single'))
                {!! Form::text("ticket_holder_questions[{$ticket->id}][$question->id]", $answer, [$question->is_required ? 'required' : '' => $question->is_required ? 'required' : '', 'class' => "ticket_holder_questions.{$ticket->id}.{$question->id}   form-control"]) !!}
            @elseif($question->question_type_id == config('attendize.question_textbox_multi'))
                {!! Form::textarea("ticket_holder_questions[{$ticket->id}][$question->id]", $answer, ['rows'=>5, $question->is_required ? 'required' : '' => $question->is_required ? 'required' : '', 'class' => "ticket_holder_questions.{$ticket->id}.{$question->id}  form-control"]) !!}
            @elseif($question->question_type_id == config('attendize.question_dropdown_single'))
                {!! Form::select("ticket_holder_questions[{$ticket->id}][$question->id]", array_merge(['' => '-- Please Select --'], $question->options->lists('name', 'name')->toArray()), $answer, [$question->is_required ? 'required' : '' => $question->is_required ? 'required' : '', 'class' => "ticket_holder_questions.{$ticket->id}.{$question->id}   form-control"]) !!}
            @elseif($question->question_type_id == config('attendize.question_dropdown_multi'))
                {!! Form::select("ticket_holder_questions[{$ticket->id}][$question->id][]",$question->options->lists('name', 'name'), $answer, [$question->is_required ? 'required' : '' => $question->is_required ? 'required' : '', 'multiple' => 'multiple','class' => "ticket_holder_questions.{$ticket->id}.{$question->id}   form-control"]) !!}
            @elseif($question->question_type_id == config('attendize.question_checkbox_multi'))
                <br>
                @foreach($question->options as $option)
                    <?php
                        $checkbox_id = md5($ticket->id.$question->id.$option->name);
                    ?>
                    <div class="custom-checkbox">
                        {!! Form::checkbox("ticket_holder_questions[{$ticket->id}][$question->id][]",$option->name, false,['class' => "ticket_holder_questions.{$ticket->id}.{$question->id}  ", 'id' => $checkbox_id]) !!}
                        <label for="{{ $checkbox_id }}">{{$option->name}}</label>
                    </div>
                @endforeach
            @elseif($question->question_type_id == config('attendize.question_radio_single'))
                <br>
                @foreach($question->options as $option)
                    <?php
                    $radio_id = md5($ticket->id.$question->id.$option->name);
                    $isAnswer = ($answer == $option->name) ? true : false;
                    ?>
                <div class="custom-radio">
                    {!! Form::radio("ticket_holder_questions[{$ticket->id}][$question->id]",$option->name, $isAnswer, ['id' => $radio_id, 'class' => "ticket_holder_questions.{$ticket->id}.{$question->id}  "]) !!}
                    <label for="{{ $radio_id }}">{{$option->name}}</label>
                </div>
                @endforeach
            @endif
        </div>
        <div class="col-xs-12">
            @if(strpos($question->title, 'enter beer into BrewMania') !== false && strtolower(trim($answer)) != 'no')
                <h3>
                    <a class="notice bold" href="//{{$brewManiaLink}}" target="_blank">Please click here to register your BrewMania entries.</a>
                </h3>
            @endif
        </div>
    </div>
    @endforeach
@endforeach

{!! Form::submit('Save', ['class' => 'btn btn-lg btn-success card-submit']) !!}
<a href="{{route('showEventQuestionsPage', ['event_id' => $event->id])}}" class="btn btn-lg btn-success">Logout</a>