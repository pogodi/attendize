<section id="questions_login" class="container">
    <div class="row">
        <h1 class='section_head'>
            Login
        </h1>
        <span class="error">{{$error}}</span>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="content">
                {!! Form::open(['url' => route('postQuestionsLogin', ['event_id' => $event->id])]) !!}

                <div class="form-group">
                    {!! Form::label('Attendee\'s Surname') !!}
                    {!! Form::text('last_name', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Attendee\'s Surname')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Ticket\'s Attendee reference (i.e. TPS1G123-1)') !!}
                    {!! Form::text('attendeeref', null,
                        array('required',
                              'class'=>'form-control',
                              'placeholder'=>'Attendee reference')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Login',
                      array('class'=>'btn btn-primary')) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>