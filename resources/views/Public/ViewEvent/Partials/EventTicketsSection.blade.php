<section id="tickets" class="container">
    <div class="row">
        <h1 class='section_head'>
            Tickets
        </h1>
    </div>

    @if($event->start_date->isPast())
    <div class="alert alert-boring">
        This event has {{($event->end_date->isFuture() ? 'already started' : 'ended')}}.
    </div>
    @else

    @if($tickets->count() > 0)

    {!! Form::open(['url' => route('postValidateTickets', ['event_id' => $event->id]), 'class' => 'ajax']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="content">
                <div class="tickets_table_wrap">
                    <table class="table">
                        <?php
                         $is_free_event = true;
                        ?>
                        @foreach($tickets as $ticket)
                        @if(!(
                            $ticket->is_paused ||
                            $ticket->sale_status === config('attendize.ticket_status_sold_out') ||
                            $ticket->sale_status === config('attendize.ticket_status_after_sale_date')
                        ))
                        <tr class="ticket" property="offers" typeof="Offer">
                            <td>
                                <span class="ticket-title semibold" property="name">
                                    {{$ticket->title}}
                                </span>
                                <p class="ticket-descripton mb0 text-muted" property="description">
                                    {{$ticket->description}}
                                </p>
                            </td>
                            <td style="width:180px; text-align: right;">
                                <div class="ticket-pricing" style="margin-right: 20px;">
                                    @if($ticket->is_free && !$ticket->use_ticket_code)
                                    FREE
                                    <meta property="price" content="0">
                                    @elseif($ticket->use_ticket_code)

                                    @else
                                        <?php
                                        $is_free_event = false;
                                        ?>
                                    <span title='{{money($ticket->price, $event->currency)}} Ticket Price + {{money($ticket->total_booking_fee, $event->currency)}} Booking Fees'>{{money($ticket->total_price, $event->currency)}} </span>
                                    <meta property="priceCurrency" content="{{ $event->currency->code }}">
                                    <meta property="price" content="{{ number_format($ticket->price, 2, '.', '') }}">
                                    @endif
                                </div>
                            </td>
                            <td style="width:85px;">
                                @if($ticket->is_paused)

                                <span class="text-danger">
                                    Currently Not On Sale
                                </span>

                                @else

                                @if($ticket->sale_status === config('attendize.ticket_status_sold_out'))
                                <span class="text-danger" property="availability" content="http://schema.org/SoldOut">
                                    Sold Out
                                </span>
                                @elseif($ticket->sale_status === config('attendize.ticket_status_before_sale_date'))
                                <span class="text-danger">
                                    Sales Have Not Started
                                </span>
                                @elseif($ticket->sale_status === config('attendize.ticket_status_after_sale_date'))
                                <span class="text-danger">
                                    Sales Have Ended
                                </span>
                                @elseif($ticket->use_ticket_code)
                                    @if(!$registered)
                                       <span class="text-danger">
                                            Already bought
                                            {!! Form::hidden('tickets[]', $ticket->id) !!}
                                            <input name="ticket_{{$ticket->id}}" type="hidden" value="1">
                                       </span>
                                    @else
                                       <span class="text-danger">
                                            Already registered
                                            {!! Form::hidden('tickets[]', $ticket->id) !!}
                                            <input name="ticket_{{$ticket->id}}" type="hidden" value="0">
                                       </span>
                                    @endif
                                @else
                                    @php
                                        $IsBought = array_search($ticket->id, $bought) !== false;
                                    @endphp
                                    @if(!$IsBought)
                                        {!! Form::hidden('tickets[]', $ticket->id) !!}
                                        <meta property="availability" content="http://schema.org/InStock">
                                        <select name="ticket_{{$ticket->id}}" class="form-control" style="text-align: center">
                                            @if ($tickets->count() > 1)
                                                <option value="0">0</option>
                                            @endif
                                            @for($i=$ticket->min_per_person; $i<=$ticket->max_per_person; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    @else
                                        Already bought
                                    @endif
                                @endif

                                @endif
                            </td>
                        </tr>
                        @endif
                        @endforeach

                        <tr class="checkout">
                            <td colspan="3">
                                @if(!$is_free_event)
                                    <div class="hidden-xs pull-left">
                                    <img class="" src="{{asset('assets/images/public/EventPage/credit-card-logos.png')}}" />
                                    @if($event->enable_offline_payments)

                                    <div class="help-block" style="font-size: 11px;">
                                        Offline Payment Methods Available
                                    </div>
                                    @endif

                                    </div>

                                @endif
                                {!!Form::submit('Continue', ['class' => 'btn btn-lg btn-primary pull-right'])!!}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
   {!! Form::hidden('is_embedded', $is_embedded) !!}
   {!! Form::close() !!}

    @else

    <div class="alert alert-boring">
        Tickets are currently unavailable.
    </div>

    @endif

    @endif

</section>